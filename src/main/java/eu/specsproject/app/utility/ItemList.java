package eu.specsproject.app.utility;

import javax.xml.bind.annotation.XmlValue;

public class ItemList {
    	@XmlValue
        public String itemValue;
    	
    	public ItemList(){
    	}
    	
    	public ItemList(String itemValue){
    		this.itemValue = itemValue;
    	}
}
