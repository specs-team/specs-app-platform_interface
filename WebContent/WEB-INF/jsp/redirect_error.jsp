<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>OAuth SPECS Client Test</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
	padding-left: 50px;
	padding-right: 50px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

</head>
<!-- ********************************-->

<body>

	<jsp:include page="../../utils/navigator_login.jsp">
		<jsp:param name="_redirect" value="class='active'" />
	</jsp:include>

	<!-- Main jumbotron for a primary marketing message or call to action -->

	<div class="tab-pane" id="tabStoreParams">
		<div class="panel panel-default">

			<div class="panel-heading">
				<h2>SPECS OAuth error!</h2>
			</div>

			<div class="panel-body ">


				<div class="well">
					Error details<br />
				</div>

				<!-- ****************************************************************************************** -->

				<div class="panel panel-default">
					<div class="panel-body">
						<b>Error:</b>${requestScope.error}
						<br /> <b>Description:</b> ${requestScope.error_description}
						<br />
					</div>
				</div>
				<br />

			</div>

		</div>
	</div>

	<jsp:include page="../../utils/footer.jsp" />

</body>
</html>