package eu.specsproject.app.restclient.frontend.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.enforcement.Component;
import eu.specs.datamodel.enforcement.SecurityMechanism;
import eu.specs.datamodel.sla.sdt.CapabilityType;
import eu.specsproject.app.model.CallResponse;
import eu.specsproject.app.model.CheckedCapability;
import eu.specsproject.app.model.ChefServerResponse;
import eu.specsproject.app.model.Collection;
import eu.specsproject.app.model.CollectionSchema;
import eu.specsproject.app.model.CollectionSchemaV2;
import eu.specsproject.app.model.Item;
import eu.specsproject.app.model.SlaDetail;
import eu.specsproject.app.model.SlasResponse;
import eu.specsproject.app.utility.BuilderExtractor;
import eu.specsproject.app.utility.Common;
import eu.specsproject.app.utility.ParseCollectionUtilities;
import eu.specsproject.app.utility.PropertiesManager;
import eu.specsproject.app.utility.Utility;

@Path("/")
public class RestResources {

	BuilderExtractor builder = new BuilderExtractor();

	//UTILITIES FOR PLATFORM CONTROL PAGE
	@GET
	@Path("/componentsData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getComponentsData(@QueryParam("section") String section){
		try {
			String fileName = "platform_components_config.json";
			if(section != null){
				if(section.equals("platform")){
					fileName = "platform_components_config.json";
				}else if(section.equals("negotiation")){
					fileName = "negotiation_components_config.json";
				}else if(section.equals("enforcement")){
					fileName = "enforcement_components_config.json";
				}else if(section.equals("monitoring")){
					fileName = "monitoring_components_config.json";
				}else if(section.equals("sla_management")){
					fileName = "sla_management_config.json";
				}else if(section.equals("vertical_layer")){
					fileName = "vertical_layer_components_config.json";
				}
			}
			ResponseBuilder response = Response.ok(Utility.readFile("configs", fileName));
			return response.build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}    
	}

	@GET
	@Path("/requestBody")
	@Produces(MediaType.TEXT_PLAIN)
	public String getPostBody(@QueryParam("file_name") String fileName){
		try {
			return Utility.readFile("test_resources",fileName);
		} catch (IOException e) {
			return "";
		}
	}

	@POST
	@Path("/submitRequest")
	@Consumes("application/x-www-form-urlencoded")
	@Produces(MediaType.APPLICATION_JSON)
	public String submitRequest(MultivaluedMap<String, String> form){
//		System.out.println("Form component: "+form.getFirst("component"));
//		System.out.println("Form path: "+form.getFirst("path"));
//		System.out.println("Form method: "+form.getFirst("method"));
//		System.out.println("Form body: "+form.getFirst("body"));
//		System.out.println("Form content-type: "+form.getFirst("content-type"));

		CallResponse callResponse  = null;

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(form.getFirst("path"));

		if(form.getFirst("method").equals("GET")){
			Response response = target.request().get();
			String entity = response.readEntity(String.class);
			List<String> urls = ParseCollectionUtilities.getUrlList(entity);
			callResponse = new CallResponse(response.getStatus()+" "+response.getStatusInfo().toString(), entity, urls);
		}else if(form.getFirst("method").equals("POST")){
			Response response = target.request().post(Entity.entity(form.getFirst("body"),form.getFirst("content-type")));
			callResponse = new CallResponse(response.getStatus()+" "+response.getStatusInfo().toString(), response.readEntity(String.class));
		}else if(form.getFirst("method").equals("DELETE")){
			Response response = target.request().delete();
			callResponse = new CallResponse(response.getStatus()+" "+response.getStatusInfo().toString(), response.readEntity(String.class));
		}

		return new Gson().toJson(callResponse);
	}

	//UTILITIES FOR SLAS MANAGEMENT PAGE
	@POST
	@Path("/submitRequestSlaManagement")
	@Consumes("application/x-www-form-urlencoded")
	@Produces(MediaType.APPLICATION_JSON)
	public String submitRequestSlaManagement(MultivaluedMap<String, String> form){
//		System.out.println("submitRequestSlaManagement called");
//		System.out.println("Form path: "+form.getFirst("path"));

		SlasResponse slasResponse  = null;

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(form.getFirst("path"));

		Response response = target.request().get();
		String slaXmlCollection = response.readEntity(String.class);
		Collection collection = builder.buildCollectionFromXml(slaXmlCollection);
		ArrayList<SlaDetail> details = new ArrayList<SlaDetail>();

		try{
//			System.out.println("Number of SLAS: "+collection.item.size());

			for (Item item : collection.item){
				SlaDetail detail = new SlaDetail();

//				System.out.println("SLA ID: "+item.id);
				detail.setId(item.id);
				//GET CUSTOMER
				Client client1 = ClientBuilder.newClient();
				WebTarget target1 = client1.target(form.getFirst("path").split("\\?")[0]+"/"+item.id+"/customer");
//				System.out.println("TARGET FOR CUSTOMER: "+target1);
				String response1 = target1.request().get(String.class);
//				System.out.println("SLA CUSTOMER: "+response1);
				detail.setCustomer(response1);

				//GET STATE
				Client client2 = ClientBuilder.newClient();
				WebTarget target2 = client2.target(form.getFirst("path").split("\\?")[0]+"/"+item.id+"/status");
//				System.out.println("TARGET FOR STATE: "+target2);
				String response2 = target2.request().get(String.class);
//				System.out.println("SLA STATE: "+response2);
				detail.setState(response2);

				details.add(detail);
			}
		}catch(NullPointerException e){}

		slasResponse = new SlasResponse(response.getStatus()+" "+response.getStatusInfo().toString(), slaXmlCollection, details);

		return new Gson().toJson(slasResponse);
	}

	@POST
	@Path("/signSla")
	@Consumes("application/x-www-form-urlencoded")
	@Produces(MediaType.APPLICATION_JSON)
	public Response signSla(MultivaluedMap<String, String> form){
//		System.out.println("signSla called");
//		System.out.println("Form slaId: "+form.getFirst("slaId"));
		String slaId = form.getFirst("slaId");

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(PropertiesManager.getProperty("sla_manager.endpoint")+"/"+slaId+"/sign");

		Response response = target.request().post(null);

		return response;
	}
	
	@POST
	@Path("/removeSla")
	@Consumes("application/x-www-form-urlencoded")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteSla(MultivaluedMap<String, String> form){
		String slaId = form.getFirst("slaId");

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(PropertiesManager.getProperty("sla_manager.endpoint")+"/"+slaId);

		Response response = target.request().delete();

		return response;
	}

	//UTILITIES FOR SPECS SERVICES MANAGEMENT PAGE
	@GET
	@Path("/getServices")
	@Produces(MediaType.APPLICATION_JSON)
	public String getServices(){

		CallResponse callResponse  = null;

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(PropertiesManager.getProperty("slo_manager.endpoint"));

		Response response = target.request().get();
		callResponse = new CallResponse(response.getStatus()+" "+response.getStatusInfo().toString(), response.readEntity(String.class));

		return new Gson().toJson(callResponse);
	}

	@GET
	@Path("/getSecurityMechanisms")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSecurityMechanisms(){

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(PropertiesManager.getProperty("service_manager.endpoint"));

		Response response = target.request().get();

		return response.readEntity(String.class);
	}

	@GET
	@Path("/getArtifacts")
	@Produces(MediaType.APPLICATION_JSON)
	public String getArtifacts(){

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(Common.BITBUCKET_RECIPES_PATH);

		Response response = target.request().get();

		return response.readEntity(String.class);
	}

	@POST
	@Path("/addSecurityService")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addSecurityService(@FormDataParam("datafile") InputStream uploadedInputStream,  @FormDataParam("datafile") FormDataContentDisposition filename1){

		String template = readStream(uploadedInputStream);

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(PropertiesManager.getProperty("slo_manager.endpoint"));

		Response response = target.request()
				.post(Entity.entity(template,MediaType.TEXT_XML));
//		System.out.println(response.getStatus());
		if(response.getStatus() == 201){
			return Response.status(response.getStatus()).entity(response.getEntity()).build();
		}
		return Response.status(response.getStatus()).build();
	}

	@POST
	@Path("/addSecurityMechanism")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addSecurityMechanism(@FormDataParam("datafile") InputStream uploadedInputStream,  @FormDataParam("datafile") FormDataContentDisposition filename1){

		String template = readStream(uploadedInputStream);

		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(PropertiesManager.getProperty("service_manager.endpoint"));

		Response response = target.request()
				.post(Entity.entity(template,MediaType.TEXT_PLAIN));
//		System.out.println(response.getStatus());
		if(response.getStatus() == 201){
			return Response.status(response.getStatus()).entity(response.getEntity()).build();
		}
		return Response.status(response.getStatus()).build();
	}

	@GET
	@Path("/checkCapabilities")
	@Produces(MediaType.APPLICATION_JSON)
	public String checkCapabilities(@QueryParam("service_id") String serviceId){
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(PropertiesManager.getProperty("slo_manager.endpoint")+"/"+serviceId);

		Response response = target.request()
				.get();
		String templateXml = response.readEntity(String.class);

		AgreementOffer offer = builder.buildOfferFromXml(templateXml);

//		System.out.println("NUMERO DI TERMS: "+offer.getTerms().getAll().getAll().size());
		List<Term> terms = offer.getTerms().getAll().getAll();

		ArrayList<CheckedCapability> checkedCapabilities = new ArrayList<CheckedCapability>();
		try{
			for (Term term : terms){
				if(term instanceof ServiceDescriptionTerm){
					ServiceDescriptionTerm serviceTerm = (ServiceDescriptionTerm) term;
					List<CapabilityType> capabilities = serviceTerm.getServiceDescription().getCapabilities().getCapability();

					for (CapabilityType capability : capabilities){
//						System.out.println("CAPABILITY: "+capability.getId());
						CheckedCapability checkedCapability = new CheckedCapability();
						checkedCapability.setCapabilityId(capability.getName());
						Client client2 = ClientBuilder.newClient();
						WebTarget target2 = client2.target(PropertiesManager.getProperty("service_manager.endpoint")).queryParam("capabilities_id", "[\""+capability.getId()+"\"]");
						Response response2 = target2.request().get();
						if(response2.getStatus() == 200){
//							System.out.println("RESPONSE ENTITY OK");
							CollectionSchema schema = new Gson().fromJson(response2.readEntity(String.class), CollectionSchema.class);
							checkedCapability.setRowStyle("list-group-item-danger");
							ArrayList<String> mechanismCover = new ArrayList<String>();
							for (int i = 0; i < schema.getItemList().size(); i++){
								checkedCapability.setRowStyle("list-group-item-success");
								mechanismCover.add(schema.getItemList().get(i).split("/")[schema.getItemList().get(i).split("/").length-1]);
							}
							checkedCapability.setMechanismsId(mechanismCover);
							checkedCapabilities.add(checkedCapability);
						}else{
//							System.out.println("RESPONSE ENTITY ERROR");
						}
					}
				}
			}
		}catch(NullPointerException e){

		}

		return new Gson().toJson(checkedCapabilities);
	}

	@GET
	@Path("/checkArtifacts")
	@Produces(MediaType.APPLICATION_JSON)
	public String checkArtifacts(@QueryParam("mechanism_id") String mechanismId){
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(PropertiesManager.getProperty("service_manager.endpoint")+"/"+mechanismId);

		Response response = target.request()
				.get();

		String mechanismJson = response.readEntity(String.class);

		SecurityMechanism mechanism = new SecurityMechanism(); 
		ArrayList<String> recipes = new ArrayList<String>();
		ObjectMapper mapper = new ObjectMapper();

		try {
			mechanism = mapper.readValue(mechanismJson, SecurityMechanism.class);

			List<Component> components = mechanism.getMetadata().getComponents();

			for (Component component : components){
				recipes.add(component.getCookbook()+":"+component.getRecipe());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new Gson().toJson(recipes);
	}

	@GET
	@Path("/updateChefServer")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateChefServer(){
		Client clientPre = ClientBuilder.newClient();

		WebTarget targetPre = clientPre.target(PropertiesManager.getProperty("update_chef.endpoint")+"?token="+PropertiesManager.getProperty("update_chef.token")+"&operation=verify");

		Response responsePre = targetPre.request()
				.get();
		String stringResponsePre = responsePre.readEntity(String.class);

		ChefServerResponse respChef = new Gson().fromJson(stringResponsePre, ChefServerResponse.class);

		if(respChef.getMessage().equals("done")){

			Client client = ClientBuilder.newClient();

			WebTarget target = client.target(PropertiesManager.getProperty("update_chef.endpoint")+"?token="+PropertiesManager.getProperty("update_chef.token")+"&operation=update");

			Response response = target.request()
					.get();
			String stringResponse = response.readEntity(String.class);

			return stringResponse;
		}else{
			ChefServerResponse respChef1 = new ChefServerResponse();
			respChef1.setMessage("Error!\nIs not possible to execute a refresh at the moment. "
					+ "A refresh operation is already ongoing!\n"
					+ "Retry later or try to click \"Check Update Status\"");

			return new Gson().toJson(respChef1);
		}
	}

	@GET
	@Path("/verifyChefServer")
	@Produces(MediaType.APPLICATION_JSON)
	public String verifyChefServer(){
		Client client = ClientBuilder.newClient();

		WebTarget target = client.target(PropertiesManager.getProperty("update_chef.endpoint")+"?token="+PropertiesManager.getProperty("update_chef.token")+"&operation=verify");

		Response response = target.request()
				.get();
		String stringResponse = response.readEntity(String.class);

		return stringResponse;
	}

	@GET
	@Path("/monitoringPage")
	@Produces(MediaType.TEXT_PLAIN)
	public String getMonitoringPage(){
		String appUrl = PropertiesManager.getProperty("specs_app.basepath");
		String[] appUrlSplit = appUrl.split(":");
		String finalUrl = appUrl;
		if(appUrlSplit.length == 3){
			finalUrl = appUrlSplit[0] + ":" + appUrlSplit[1];
		}
		return finalUrl+":81/mos/run/wui/";
	}
	
	//UTILITIES
	private String readStream(InputStream inputStream) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(inputStream));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();
	}
}
