<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA Management</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link rel='stylesheet'
	href='https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.8.0/loading-bar.min.css'
	type='text/css' media='all' />
<link rel='stylesheet'
	href='bootstrap/css/my-loading-bar.css'
	type='text/css' media='all' />
	
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}

.space {
	padding: 10px;
}
</style>

</head>
<!-- ********************************-->

<body ng-app="SPECS-SLAS-MANAGEMENT">

	<jsp:include page="utils/navigator_owner.jsp">
		<jsp:param name="_img_path" value="src='img/specs-logo-32.png'" />
		<jsp:param name="_slas_management" value="class='active'" />
	</jsp:include>

	<div class='container' ng-controller="slasManagementController">

		<div class="page-header">
			<h1>SPECS SLAs management</h1>
		</div>

		<div class="tab-pane" id="tabStoreParams">

			<!-- API CALL -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2>Call Specification</h2>
				</div>
				<div class="panel-body ">

					<table style="width: 100%;">
						<tr>
							<td style="width: 35%;">
								<div class="well" style="width: 100%;">
									<b>Type of request</b>
								</div>
							</td>
							<td style="width: 5%;"></td>
							<td style="width: 60%;">
								<div class="well" style="width: 100%;">
									<b>Query Parameter</b>
								</div>
							</td>
						</tr>
						<tr>
							<td style="width: 35%;"><select class="form-control"
								name="typeOfRequest" id="selectedTypeOfRequest"
								ng-options="option.name for option in typeOfRequests track by option.name"
								ng-model="typeOfRequests.selectedTypeOfRequest"
								style="width: 100%;" ng-change="updateScreen('typeOfRequest')"></select></td>
							<td style="width: 5%;"></td>
							<td style="width: 60%;"><select class="form-control"
								name="queryParam" id="selectedQueryParam"
								ng-options="option for option in typeOfRequests.selectedTypeOfRequest.url_params.values track by option"
								ng-model="typeOfRequests.selectedTypeOfRequest.selectedQueryParam"
								style="width: 100%;" ng-change="updateScreen('queryParameter')"></select></td>
						</tr>
					</table>
					<br />
					<table style="width: 100%;">
						<tr>
							<td>
								<div style="width: 100%;">
									<b>URL </b>
								</div>
							</td>
							<td style="width: 1%;"></td>
							<td style="width: 95%;">
								<div style="width: 100%;">
									<input type="text" class="form-control" placeholder="URL path"
										id="url-path-id" readonly
										value="{{typeOfRequests.selectedTypeOfRequest.base_path}}?{{typeOfRequests.selectedTypeOfRequest.url_params.key}}={{typeOfRequests.selectedTypeOfRequest.selectedQueryParam}}">
								</div>
							</td>
						</tr>
					</table>
					<br />
					<div ng-show="contentTypeEnabled">
						<table style="width: 100%;">
							<tr>
								<td><b>CONTENT TYPE: </b></td>
								<td style="width: 25%;"><input type="radio"
									ng-model="typeOfRequests.selectedTypeOfRequest.selectedPath.methods.selectedMethod.content_type"
									value="text/xml"> text/xml </input></td>
								<td style="width: 25%;"><input type="radio"
									ng-model="typeOfRequests.selectedTypeOfRequest.selectedPath.methods.selectedMethod.content_type"
									value="application/json"> application/json</input></td>
								<td style="width: 25%;"><input type="radio"
									ng-model="typeOfRequests.selectedTypeOfRequest.selectedPath.methods.selectedMethod.content_type"
									value="text/plain"> text/plain </input></td>
							</tr>
						</table>

					</div>
					<br />
					<div
						ng-show="typeOfRequests.selectedTypeOfRequest.selectedPath.methods.selectedMethod.type=='POST' || typeOfRequests.selectedTypeOfRequest.selectedPath.methods.selectedMethod.type=='UPDATE'">
						<div>
							<b>BODY:</b>
						</div>
						<div>
							<textarea id="body-value-id" rows="10" style="width: 100%;">{{typeOfRequests.selectedTypeOfRequest.selectedPath.methods.selectedMethod.body_file}}</textarea>
						</div>
						<br />
					</div>
					<div>
						<button class="btn btn-primary" ng-click="submitRequest()">SUBMIT
							REQUEST</button>
					</div>
				</div>

			</div>

			<!-- CALL RESPONSE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2>Response</h2>
				</div>
				<div class="panel-body " ng-show="statusValue">
					<div>
						<table style="width: 100%;">
							<tr>
								<td style="width: 20%;"><b>Status</b></td>
								<td style="width: 1%;"></td>
								<td style="width: 20%;"><div>{{statusValue}}</div></td>
								<td style="width: 5%;"></td>
								<td style="width: 20%;"><b>Type of visualization</b></td>
								<td style="width: 1%;"></td>
								<td style="width: 20%;"><select class="form-control"
									name="visualizationType" id="visualization-type-id"
									ng-options="option for option in responseType.availableTypes track by option"
									ng-model="responseType.selectedResponseType"
									ng-change="updateVisualizationType()"></select></td>
								<td style="width: 13%;"></td>
							</tr>
						</table>
					</div>
					<br />
					<div>
						<div ng-show="visualization_type=='XML'" id="response-body-xml"></div>
						<div ng-show="visualization_type=='RAW'" id="response-body-raw">{{responseValue}}</div>
						<div ng-show="visualization_type=='JSON'" id="response-body-json">
							<pre>{{responseJson}}</pre>
						</div>
						<div ng-show="visualization_type=='PRETTY'"
							id="response-body-pretty">
							<table style="width: 100%;">
								<tr style="width: 100%;">
									<td style="width: 32%;">
										<div class="well" style="width: 100%;">
											<b>SLA ID</b>
										</div>
									</td>
									<td style="width: 2%;"></td>
									<td style="width: 32%;">
										<div class="well" style="width: 100%;">
											<b>CUSTOMER</b>
										</div>
									</td>
									<td style="width: 2%;"></td>
									<td style="width: 32%;">
										<div class="well" style="width: 100%;">
											<b>SLA STATE</b>
										</div>
									</td>
									<td
										ng-show="typeOfRequests.selectedTypeOfRequest.selectedQueryParam=='NEGOTIATING'"
										style="width: 2%;"></td>
									<td
										ng-show="typeOfRequests.selectedTypeOfRequest.selectedQueryParam=='NEGOTIATING'"
										style="width: 32%;"></td>
								</tr>

								<tr style="width: 100%;"
									ng-repeat="responsePrettyOne in responsePretty">
									<td style="width: 32%;">
										<div class="panel-body" style="width: 100%;">
											<b>{{responsePrettyOne.id}}</b>
										</div>
									</td>
									<td style="width: 2%;"></td>
									<td style="width: 32%;">
										<div class="panel-body" style="width: 100%;">
											<b>{{responsePrettyOne.customer}}</b>
										</div>
									</td>
									<td style="width: 2%;"></td>
									<td style="width: 32%;">
										<div class="panel-body" style="width: 100%;">
											<b>{{responsePrettyOne.state}}</b>
										</div>
									</td>
									<td ng-show="responsePrettyOne.state=='NEGOTIATING'"
										style="width: 2%;"><div class="space"
											style="width: 100%;">
											<b><br></b>
										</div></td>
									<td ng-show="responsePrettyOne.state=='NEGOTIATING'"
										style="width: 32%;">
										<button class="btn btn-primary"
											ng-click="signSla(responsePrettyOne.id)">SIGN SLA</button>
									</td>
									<td ng-show="responsePrettyOne.state=='PENDING'"
										style="width: 2%;"><div class="space"
											style="width: 100%;">
											<b><br></b>
										</div></td>
									<td ng-show="responsePrettyOne.state=='PENDING'"
										style="width: 32%;">
										<button class="btn btn-primary"
											ng-click="deleteSla(responsePrettyOne.id)">DELETE SLA</button>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<jsp:include page="utils/footer.jsp" />

	<!-- JAVASCRIPT / ANGULAR  -->
	<script src="js/angular.js"></script>
	<script
	src="js/angular-animate.js"></script>
	
	<script src="js/XMLDisplay.js"></script>
	<script
		src='https://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.8.0/loading-bar.min.js'></script>

	<script>
		var app = angular.module('SPECS-SLAS-MANAGEMENT', [
				'angular-loading-bar', 'ngAnimate' ]);

		app.config([ 'cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
			cfpLoadingBarProvider.includeSpinner = false;
		} ]);

		app
				.controller(
						'slasManagementController',
						[
								'$scope',
								'$http',
								'$filter',
								'cfpLoadingBar',
								function($scope, $http, $filter, cfpLoadingBar) {
									console.log("angular init");

									$scope.contentTypeEnabled = false;

									$http(
											{
												method : 'GET',
												url : 'services/rest/componentsData?section=sla_management'
											})
											.then(
													function successCallback(
															response) {
														console
																.log(response.data);
														$scope.typeOfRequests = response.data.typeOfRequests;
														setDefaultValues();
													},
													function errorCallback(
															response) {

													});

									function setDefaultValues() {
										$scope.typeOfRequests.selectedTypeOfRequest = $scope.typeOfRequests[0];
										$scope.typeOfRequests.selectedTypeOfRequest.selectedQueryParam = $scope.typeOfRequests.selectedTypeOfRequest.url_params.values[0];
									}

									$scope.updateScreen = function(selectedType) {
										console.log("UPDATED TYPE: "
												+ selectedType);
										if (selectedType == 'typeOfRequest') {
											$scope.typeOfRequests.selectedTypeOfRequest.selectedQueryParam = $scope.typeOfRequests.selectedTypeOfRequest.url_params.values[0];
										}
									}

									$scope.submitRequest = function() {
										console.log("SUBMIT CALLED")
										cfpLoadingBar.start();
										
										$http(
												{
													method : 'POST',
													url : 'services/rest/submitRequestSlaManagement',
													headers : {
														'Content-Type' : 'application/x-www-form-urlencoded'
													},
													transformRequest : function(
															obj) {
														var str = [];
														for ( var p in obj)
															str
																	.push(encodeURIComponent(p)
																			+ "="
																			+ encodeURIComponent(obj[p]));
														return str.join("&");
													},
													data : {
														'path' : document
																.getElementById('url-path-id').value,
													}

												})
												.then(
														function successCallback(
																response) {
															console
																	.log(response);
															console
																	.log(response.status);

															if (response.status == '200') {
																$scope.responseRaw = response.data;
																$scope.statusValue = $scope.responseRaw.status;
																$scope.responseValue = $scope.responseRaw.rowResponse;

																$scope.responsePretty = $scope.responseRaw.slaDetails;
															} else {
																$scope.statusValue = response.status
																		+ " "
																		+ response.statusText
																$scope.responseValue = $scope.responseRaw.data
															}

															$scope
																	.updateVisualizationType();

														},
														function errorCallback(
																response) {
															console
																	.log(response);
															console
																	.log(response.status);
															console
																	.log(response.data);
															$scope.statusValue = response.status
																	+ " "
																	+ response.statusText
															$scope.responseValue = response.data;
															$scope
																	.updateVisualizationType();
														});
									}

									$scope.signSla = function(slaId) {
										console.log("SIGN CALLED - " + slaId);
										$http(
												{
													method : 'POST',
													url : 'services/rest/signSla',
													headers : {
														'Content-Type' : 'application/x-www-form-urlencoded'
													},
													transformRequest : function(
															obj) {
														var str = [];
														for ( var p in obj)
															str
																	.push(encodeURIComponent(p)
																			+ "="
																			+ encodeURIComponent(obj[p]));
														return str.join("&");
													},
													data : {
														'slaId' : slaId,
													}

												})
												.then(
														function successCallback(
																response) {
															console
																	.log(response);
															console
																	.log(response.status);

															if (response.status == '204') {
																alert("The Sla is correctly signed, the end user can implement it now!");
																$scope
																		.submitRequest();
															} else {
																alert("Error: the Sla is not signed! - Description: "
																		+ response.data);
															}

														},
														function errorCallback(
																response) {
															console
																	.log(response);
															console
																	.log(response.status);
															console
																	.log(response.data);
															alert("Error "
																	+ response.status
																	+ ": the Sla is not signed! - Description: "
																	+ response.data);

														});
									}
									
									$scope.deleteSla = function(slaId) {
										console.log("DELETE CALLED - " + slaId);
										$http(
												{
													method : 'POST',
													url : 'services/rest/removeSla',
													headers : {
														'Content-Type' : 'application/x-www-form-urlencoded'
													},
													transformRequest : function(
															obj) {
														var str = [];
														for ( var p in obj)
															str
																	.push(encodeURIComponent(p)
																			+ "="
																			+ encodeURIComponent(obj[p]));
														return str.join("&");
													},
													data : {
														'slaId' : slaId,
													}

												})
												.then(
														function successCallback(
																response) {
															console
																	.log(response);
															console
																	.log(response.status);

															if (response.status == '200') {
																alert("The Sla "+response.data+" is correctly deleted!");
																$scope
																		.submitRequest();
															} else {
																alert("Error: the Sla is not deleted! - Description: "
																		+ response.data);
															}

														},
														function errorCallback(
																response) {
															console
																	.log(response);
															console
																	.log(response.status);
															console
																	.log(response.data);
															alert("Error "
																	+ response.status
																	+ ": the Sla is not deleted! - Description: "
																	+ response.data);

														});
									}

									$scope.responseType = {
										availableTypes : [ 'RAW', 'XML',
												'JSON', 'PRETTY' ],
										selectedResponseType : 'PRETTY'
									};

									$scope.updateVisualizationType = function() {
										if ($scope.responseType.selectedResponseType == 'XML') {
											LoadXMLString('response-body-xml',
													$scope.responseValue);
										} else if ($scope.responseType.selectedResponseType == 'JSON') {
											$scope.responseJson = JSON
													.stringify(
															JSON
																	.parse($scope.responseValue),
															undefined, 4);
										}
										$scope.visualization_type = $scope.responseType.selectedResponseType;
									}

								} ]);
	</script>
</body>
</html>