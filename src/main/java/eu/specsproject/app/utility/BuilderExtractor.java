package eu.specsproject.app.utility;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import eu.specs.datamodel.agreement.offer.AgreementOffer;
//import eu.specs.datamodel.sla.sdt.NISTcontrol;
//import eu.specs.datamodel.sla.sdt.CapabilityType.ControlFramework;

public class BuilderExtractor {

	public BuilderExtractor() {
		
	}


	public AgreementOffer buildOfferFromXml(String xml) {
		AgreementOffer offer = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(AgreementOffer.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			offer = (AgreementOffer) unmarshaller
					.unmarshal(new StringReader(xml));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return offer;
	}
	
	public eu.specsproject.app.model.Collection buildCollectionFromXml(String xml) {
		eu.specsproject.app.model.Collection collection = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(eu.specsproject.app.model.Collection.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			collection = (eu.specsproject.app.model.Collection) unmarshaller
					.unmarshal(new StringReader(xml));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return collection;
	}
	
	public Collection buildCollectionTypeFromXml(String xml) {
		Collection collection = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(Collection.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			collection = (Collection) unmarshaller
					.unmarshal(new StringReader(xml));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return collection;
	}
	
	
}
