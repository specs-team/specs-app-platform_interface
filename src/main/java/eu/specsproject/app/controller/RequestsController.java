package eu.specsproject.app.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthAccessTokenResponse;
import org.apache.oltu.oauth2.client.response.OAuthAuthzResponse;
import org.apache.oltu.oauth2.client.response.OAuthClientResponse;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.apache.oltu.oauth2.common.message.types.ResponseType;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import eu.specsproject.app.model.UserProfile;
import eu.specsproject.app.utility.Common;
import eu.specsproject.app.utility.PropertiesManager;


@Controller
@Scope("session")
public class RequestsController {

	@RequestMapping(value="/login",method=RequestMethod.GET)
	public void login(@Context HttpServletRequest request) {
		System.out.println("LOGIN GET CALLED");
		System.out.println("OAUTH BASEPATH: "+PropertiesManager.getProperty("oauth_server.basepath"));
		
		request.setAttribute("authorize_path", PropertiesManager.getProperty("authorization.endpoint")+"?"+
												"client_id="+Common.CLIENT_ID+"&"+
												"redirect_uri="+PropertiesManager.getProperty("redirect_uri.path")+"&"+
												"response_type="+Common.RESPONSE_TYPE_CODE+"&"+
												"state="+Common.STATE);
	}
	
	@RequestMapping(value="/home")
	public ModelAndView redirectReq(ModelMap model, @Context HttpServletRequest request){
		System.out.println("Redirect called from oAuth server");
		try {
			//Get the code form the request
			OAuthClientResponse resp = OAuthAuthzResponse.oauthCodeAuthzResponse(request);
			String code = resp.getParam(ResponseType.CODE.toString());
			System.out.println("Received code: "+code);
			request.setAttribute("code", code);

			//Build the request to get the token using the authorization_code grant type
			OAuthClientRequest requestOauth = OAuthClientRequest
					.tokenLocation(PropertiesManager.getProperty("token.endpoint"))
					.setGrantType(GrantType.AUTHORIZATION_CODE)
					.setRedirectURI(PropertiesManager.getProperty("redirect_uri.path"))
					.setCode(code)
					.buildBodyMessage();

			//Add Authorization header to the request (not supported in Apache Oltu)
			byte[]   bytesEncoded = org.apache.commons.codec.binary.Base64.encodeBase64((Common.CLIENT_ID+":"+Common.CLIENT_SECRET).getBytes());
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", "Basic "+new String(bytesEncoded));
			headers.put("Content-Type", "application/x-www-form-urlencoded");

			//Send request
			URLConnectionClient urlConnectionClient = new URLConnectionClient();
			OAuthAccessTokenResponse oAuthResponse = urlConnectionClient.execute(requestOauth, headers,
					"POST", OAuthJSONAccessTokenResponse.class);
			String token = oAuthResponse.getAccessToken();
			System.out.println("Received token: "+token);
			request.getSession().setAttribute("token", token);
			Common.access_token = token;

			//Request the user profile
			HttpURLConnection httpURLConnection = doGetWithTokenRequest(PropertiesManager.getProperty("profile.endpoint"), token);
			String profile = parseResponse(httpURLConnection);
			UserProfile userProfile = new  Gson().fromJson(profile, UserProfile.class);
			request.setAttribute("profile_username", userProfile.getUsername());
			request.setAttribute("profile_role", userProfile.getRole());
			
			if(userProfile.getRole().equals("owner")){
				return new ModelAndView("owner_home");
			}
			request.setAttribute("token", token);
			
			System.out.println("Reidrect call: "+PropertiesManager.getProperty("user_dashboard.path")+"?token="+token);
			return new ModelAndView("redirect:" + PropertiesManager.getProperty("user_dashboard.path")+"?token="+token);
		} catch (OAuthProblemException e) {
			e.printStackTrace();
			request.setAttribute("error", e.getError());
			request.setAttribute("error_description", e.getDescription());
			return new ModelAndView("redirect_error");
		} catch (OAuthSystemException e) {
			e.printStackTrace();
			request.setAttribute("error", e.getCause());
			request.setAttribute("error_description", e.getMessage());
			return new ModelAndView("redirect_error");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			request.setAttribute("error", e.getCause());
			request.setAttribute("error_description", e.getMessage());
			return new ModelAndView("redirect_error");
		} catch (IOException e) {
			e.printStackTrace();
			request.setAttribute("error", e.getCause());
			request.setAttribute("error_description", e.getMessage());
			return new ModelAndView("redirect_error");
		}
	}
	
	@RequestMapping(value="/redirect_error",method=RequestMethod.GET)
	public void redirectErrorReq(@Context HttpServletRequest request){
		System.out.println("Redirect error called");
	}
	
	@RequestMapping(value="/owner_home",method=RequestMethod.GET)
	public void ownerHome(@Context HttpServletRequest request) {
		System.out.println("OWNER HOME GET CALLED");
		
	}
	
	@RequestMapping(value="/logout",method=RequestMethod.GET)
	public String logout(@Context HttpServletRequest request) {
		System.out.println("LOGOUT CALLED");
		
//		Object token = request.getSession().getAttribute("token");
//		if(token != null){
//			HttpURLConnection httpURLConnection;
//			try {
//				httpURLConnection = doGetWithToken2Request(PropertiesManager.getProperty("revoke_token.endpoint"), token.toString());
//				System.out.println("Token revoke response code: "+httpURLConnection.getResponseCode());
//				
//				BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
//				StringBuilder sb = new StringBuilder();
//				String output;
//				while ((output = br.readLine()) != null) {
//					sb.append(output);
//				}
//				String response = sb.toString();
//
//				System.out.println("Token revoke response body: "+response);
//
//				request.getSession().setAttribute("token", null);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
		
		return ("redirect:"+PropertiesManager.getProperty("oauth_server_index.endpoint")+"?redirect_url="+PropertiesManager.getProperty("platform_interface.basepath")+"/platform-interface");
	}
	
	//UTILITIES
	public static HttpURLConnection doGetWithTokenRequest(String urlPath, String token) throws IOException {
		URL url = new URL(urlPath);
		URLConnection c = url.openConnection();

		if (c instanceof HttpURLConnection) {
			HttpURLConnection httpURLConnection = (HttpURLConnection)c;
			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setAllowUserInteraction(false);
			httpURLConnection.setRequestProperty("Authorization", "bearer "+token);
			httpURLConnection.connect();

			return httpURLConnection;
		}
		return null;
	}
	
	public static HttpURLConnection doGetWithToken2Request(String urlPath, String token) throws IOException {
		URL url = new URL(urlPath);
		URLConnection c = url.openConnection();

		if (c instanceof HttpURLConnection) {
			HttpURLConnection httpURLConnection = (HttpURLConnection)c;
			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setAllowUserInteraction(false);
			httpURLConnection.setRequestProperty("Authorization", "bearer "+token);
			httpURLConnection.connect();

			return httpURLConnection;
		}
		return null;
	}
	
	public static String parseResponse(HttpURLConnection httpURLConnection) throws IOException{
		int responseCode = httpURLConnection.getResponseCode();
		System.out.println("RESPONSE CODE: "+responseCode);
		if(responseCode == 200){
			BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getInputStream())));
			StringBuilder sb = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			String profile = sb.toString();

			System.out.println("PROFILE RESPONSE: "+profile);
			return profile;
		}else{
			BufferedReader br = new BufferedReader(new InputStreamReader((httpURLConnection.getErrorStream())));
			StringBuilder sb = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
			String profile = sb.toString();

			System.out.println("PROFILE RESPONSE: "+profile);
			return profile;
		}	
	}

}
