<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<style>
.dropdown-menu>li.kopie>a {
	padding-left: 5px;
}

.dropdown-submenu {
	position: relative;
}

.dropdown-submenu>.dropdown-menu {
	top: 0;
	left: 100%;
	margin-top: -6px;
	margin-left: -1px;
	-webkit-border-radius: 0 6px 6px 6px;
	-moz-border-radius: 0 6px 6px 6px;
	border-radius: 0 6px 6px 6px;
}

.dropdown-submenu>a:after {
	border-color: transparent transparent transparent #333;
	border-style: solid;
	border-width: 5px 0 5px 5px;
	content: " ";
	display: block;
	float: right;
	height: 0;
	margin-right: -10px;
	margin-top: 5px;
	width: 0;
}

.dropdown-submenu:hover>a:after {
	border-left-color: #555;
}

.dropdown-menu>li>a:hover, .dropdown-menu>.active>a:hover {
	text-decoration: underline;
}

@media ( max-width : 767px) {
	.navbar-nav {
		display: inline;
	}
	.navbar-default .navbar-brand {
		display: inline;
	}
	.navbar-default .navbar-toggle .icon-bar {
		background-color: #fff;
	}
	.navbar-default .navbar-nav .dropdown-menu>li>a {
		color: red;
		background-color: #ccc;
		border-radius: 4px;
		margin-top: 2px;
	}
	.navbar-default .navbar-nav .open .dropdown-menu>li>a {
		color: #333;
	}
	.navbar-default .navbar-nav .open .dropdown-menu>li>a:hover,
		.navbar-default .navbar-nav .open .dropdown-menu>li>a:focus {
		background-color: #ccc;
	}
	.navbar-nav .open .dropdown-menu {
		border-bottom: 1px solid white;
		border-radius: 0;
	}
	.dropdown-menu {
		padding-left: 10px;
	}
	.dropdown-menu .dropdown-menu {
		padding-left: 20px;
	}
	.dropdown-menu .dropdown-menu .dropdown-menu {
		padding-left: 30px;
	}
	li.dropdown.open {
		border: 0px solid red;
	}
}

@media ( min-width : 768px) {
	ul.nav li:hover>ul.dropdown-menu {
		display: block;
	}
	#navbar {
		text-align: center;
	}
}
</style>
<script
	src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

<div id="_navigator">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<!-- <a class="navbar-brand" href="#"> <img alt="Brand"
					src="img/specs-logo-32.png"></a>  -->

				<a class="navbar-brand" href="./"> <img alt="Brand"
					${param._img_path}></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li ${param._home}><a href="./owner_home.jsp">Home</a></li>

					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Platform Control<span class="caret"></span></a>
						<ul class="dropdown-menu">

							<li ${param._platform}><a
								href="./platform_control_platform.jsp">Platform</a></li>
							<li role="separator" class="divider"></li>

							<li ${param._negotiation}><a
								href="./platform_control_negotiation.jsp">Negotiation</a></li>
							<li role="separator" class="divider"></li>

							<li ${param._enforcement}><a
								href="./platform_control_enforcement.jsp">Enforcement</a></li>
							<li role="separator" class="divider"></li>

							<li ${param._monitoring}><a
								href="http://127.0.0.1:81/mos/run/wui/"
								target="_blank">Monitoring Dashboard</a></li>
							<li role="separator" class="divider"></li>

							<li class="dropdown dropdown-submenu"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown">Vertical
									Layer</a>
								<ul class="dropdown-menu">
									<li ${param._vertical_layer}><a
										href="./platform_control_vertical_layer.jsp">Audit</a></li>
								</ul></li>



						</ul></li>

					<li ${param._slas_management}><a href="./slas_management.jsp">SLAs
							Management</a></li>

					<li ${param._services_management}><a
						href="./services_management.jsp">SPECS Services Management</a></li>


				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li><a href="./logout.jsp">LogOut</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->


		</div>
	</nav>
</div>