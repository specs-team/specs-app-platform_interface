<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS Platform Interface</title>


<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

</head>
<!-- ********************************-->

<body>

	<jsp:include page="../../utils/navigator_owner.jsp">
		<jsp:param name="_img_path" value="src='img/specs-logo-32.png'"/>
		<jsp:param name="_home" value="class='active'" />
	</jsp:include>


	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<h1>Owner Dashboard</h1>
			<p>
				Welcome to the SPECS Platform Interface - Owner dashboard. <br /> This
				dashboard is useful to manage the SPECS platform components. In particular you can:
			<ol>
				<li>Execute all the CRUD operation on the components through the Platform Control section</li>
				<li>See all the SLAs and their status through the SLAs Management section</li>
				<li>Manage and offer a new type of Security Service through the SPECS Services Management section</li>
			</ol>
			<p>Use the tabs on the navigation bar to select the desired operation!</p>
		</div>
	</div>


	<jsp:include page="../../utils/footer.jsp" />

</body>
</html>