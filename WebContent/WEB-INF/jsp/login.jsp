<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS Platform Interface</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
	padding-left: 50px;
	padding-right: 50px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

</head>
<!-- ********************************-->

<body>

	<jsp:include page="../../utils/navigator_login.jsp">
		<jsp:param name="_login" value="class='active'" />
	</jsp:include>

	<div class="tab-pane" id="tabStoreParams">
		<div class="panel panel-default">

			<div class="panel-heading">
				<h2>To LogIn into SPECS follow these steps</h2>
			</div>

			<div class="panel-body ">


				<div class="well">
					<ol>
						<li>Click the "Login with SPECS" button to execute the login</li>
						<li>Insert your SPECS username and password</li>
						<li>Authorize the Application to read your SPECS
							profile</li>
					</ol>
				</div>
				<br />

				<p>
					<a class="btn btn-primary btn-large"
						href=<%=request.getAttribute("authorize_path")%> target="_self">LogIn
						with SPECS</a>
				</p>

			</div>

		</div>
	</div>

	<jsp:include page="../../utils/footer.jsp" />

</body>
</html>