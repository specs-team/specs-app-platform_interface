package eu.specsproject.app.model;

import javax.xml.bind.annotation.XmlValue;

public class ItemList {
    	@XmlValue
        public String itemValue;
    	
    	public ItemList(String itemValue){
    		this.itemValue = itemValue;
    	}
}
