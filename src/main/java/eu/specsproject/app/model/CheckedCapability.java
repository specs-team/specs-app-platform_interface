package eu.specsproject.app.model;

import java.util.ArrayList;

public class CheckedCapability {
	
	private String capabilityId;
	private ArrayList<String> mechanismsId;
	private String rowStyle;
	
	public String getCapabilityId() {
		return capabilityId;
	}
	public void setCapabilityId(String capabilityId) {
		this.capabilityId = capabilityId;
	}
	public ArrayList<String> getMechanismsId() {
		return mechanismsId;
	}
	public void setMechanismsId(ArrayList<String> mechanismsId) {
		this.mechanismsId = mechanismsId;
	}
	public String getRowStyle() {
		return rowStyle;
	}
	public void setRowStyle(String rowStyle) {
		this.rowStyle = rowStyle;
	}


}
