package eu.specsproject.app.utility;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import eu.specsproject.app.model.CollectionSchema;
import eu.specsproject.app.model.CollectionSchemaV2;

public class ParseCollectionUtilities {

	static public List<String> getUrlList (String entity){
		System.out.println("Entity to parse: "+entity);
		ArrayList<String> urls = new ArrayList<String>();
		eu.specsproject.app.model.Collection collection = checkCollectionXML(entity);
		if(collection == null || collection.item == null){
			System.out.println("The collection is not an XML Collection");
			CollectionSchema collectionSchema = checkCollectionSchema(entity);
			if(collectionSchema == null){
				System.out.println("The collection is not a json schema Collection");
				CollectionSchemaV2 collectionSchemaV2 = checkCollectionSchemaV2(entity);
				if(collectionSchemaV2 == null){
					System.out.println("The collection is not a json schemaV2 Collection");
					Collection collectionType = checkCollectionTypeXML(entity);
					if(collectionType == null || collectionType.itemList == null){
						System.out.println("The collection is not an XML Collection Type");
						System.out.println("PARSE ERROR! Collection Type not found!");
					}else{
						for (int i = 0; i < collectionType.itemList.size(); i++){
							urls.add(collectionType.itemList.get(i).itemValue);
						}
					}
				}else{
					for (int i = 0; i < collectionSchemaV2.getItemList().size(); i++){
						urls.add(collectionSchemaV2.getItemList().get(i).getItem());
					}
				}
			}else{
				for (int i = 0; i < collectionSchema.getItemList().size(); i++){
					urls.add(collectionSchema.getItemList().get(i));
				}
			}
		}else{
			for (int i = 0; i < collection.item.size(); i++){
				urls.add(collection.item.get(i).itemValue);
			}
		}
		return urls;
	}

	static private CollectionSchema checkCollectionSchema(String entity){
		try{
			CollectionSchema schema = new Gson().fromJson(entity, CollectionSchema.class);
			if(schema.getItemList() != null){
				return schema;
			}else{
				return null;
			}
		}catch(Exception e){
			return null;
		}

	}

	static private CollectionSchemaV2 checkCollectionSchemaV2(String entity){
		try{
			CollectionSchemaV2 schema = new Gson().fromJson(entity, CollectionSchemaV2.class);
			if(schema.getItemList() != null){
				return schema;
			}else{
				return null;
			}
		}catch(Exception e){
			return null;
		}
	}
	
	static private eu.specsproject.app.model.Collection checkCollectionXML(String entity){
		try{
			BuilderExtractor builder = new BuilderExtractor();
			eu.specsproject.app.model.Collection collection = builder.buildCollectionFromXml(entity);
			return collection;
		}catch(Exception e){
			return null;
		}
	}
	
	static private Collection checkCollectionTypeXML(String entity){
		try{
			BuilderExtractor builder = new BuilderExtractor();
			Collection collection = builder.buildCollectionTypeFromXml(entity);
			return collection;
		}catch(Exception e){
			return null;
		}
	}
}
