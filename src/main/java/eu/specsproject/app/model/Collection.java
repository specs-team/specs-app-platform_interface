/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
*/

package eu.specsproject.app.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Collection {

    @XmlAttribute
    public String resource;
    
    @XmlAttribute
    public int total;
    
    @XmlAttribute
    public int members;
    
    @XmlElement
    public List<Item> item;
    
    public Collection(){
        //Zero arguments constructor
    }
    
    public Collection(String resource, int total, int members, List<Item> item){
        this.resource=resource;
        this.total=total;
        this.members=members;
        this.item = item;
    }
}
