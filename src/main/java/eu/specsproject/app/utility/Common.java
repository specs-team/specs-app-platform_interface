package eu.specsproject.app.utility;

public class Common {
	public static final String CLIENT_ID = "end-user-app";
	public static final String CLIENT_SECRET = "end-user-app-secret";
	public static final String ACTION="view";
	
	public static final String RESPONSE_TYPE_CODE = "code";
	public static final String STATE = "abcdef";
		
	public static String access_token = "";
	
	public static final String BITBUCKET_RECIPES_PATH = "https://api.bitbucket.org/1.0/repositories/specs-team/specs-core-enforcement-repository/src/master/repository_descriptor.json";
}
