<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS Platform Control</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">

<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

</head>
<!-- ********************************-->

<body ng-app="SPECS-REST-NEGOTIATION">

	<jsp:include page="utils/navigator_owner.jsp">
				<jsp:param name="_img_path" value="src='img/specs-logo-32.png'"/>
		<jsp:param name="_negotiation" value="class='active'" />
	</jsp:include>

	<div class='container' ng-controller="negotiationController">

		<div class="page-header">
			<h1>SPECS Negotiation Rest API</h1>
		</div>

		<div class="tab-pane" id="tabStoreParams">

			<!-- API CALL -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2>Call Specification</h2>
				</div>
				<div class="panel-body ">

					<table style="width: 100%;">
						<tr>
							<td style="width: 35%;">
								<div class="well" style="width: 100%;">
									<b>Component to test</b>
								</div>
							</td>
							<td style="width: 5%;"></td>
							<td style="width: 60%;">
								<div class="well" style="width: 100%;">
									<b>API Path</b>
								</div>
							</td>
						</tr>
						<tr>
							<td style="width: 35%;"><select class="form-control"
								name="componentsName" id="selectedComponent"
								ng-options="option.name for option in components track by option.name"
								ng-model="components.selectedComponent" style="width: 100%;"
								ng-change="updateScreen('component')"></select></td>
							<td style="width: 5%;"></td>
							<td style="width: 60%;"><select class="form-control"
								name="pathsName" id="selectedPath"
								ng-options="option.path for option in components.selectedComponent.requests track by option.path"
								ng-model="components.selectedComponent.selectedPath"
								style="width: 100%;" ng-change="updateScreen('path')"></select></td>
						</tr>
					</table>
					<br />
					<table style="width: 100%;">
						<tr>
							<td>
								<div style="width: 100%;">
									<b>URL </b>
								</div>
							</td>
							<td style="width: 1%;"></td>
							<td style="width: 80%;">
								<div style="width: 100%;">
									<input type="text" class="form-control" placeholder="URL path"
										id="url-path-id"
										ng-model = urlValue>
								</div>
							</td>
							<td style="width: 3%;"></td>
							<td>
								<div style="width: 100%;">
									<b>METHOD </b>
								</div>
							</td>
							<td style="width: 1%;"></td>
							<td style="width: 15%;"><select class="form-control"
								name="methodsName" id="selected-method-id"
								ng-options="option.type for option in components.selectedComponent.selectedPath.methods track by option.type"
								ng-model="components.selectedComponent.selectedPath.methods.selectedMethod"
								style="width: 100%;" ng-change="updateScreen('method')"></select></td>
						</tr>
					</table>
					<br />
					<div ng-show="contentTypeEnabled">
						<table style="width: 100%;">
							<tr>
								<td><b>CONTENT TYPE: </b></td>
								<td style="width: 25%;"><input type="radio"
									ng-model="components.selectedComponent.selectedPath.methods.selectedMethod.content_type"
									value="text/xml"> text/xml </input></td>
								<td style="width: 25%;"><input type="radio"
									ng-model="components.selectedComponent.selectedPath.methods.selectedMethod.content_type"
									value="application/json"> application/json</input></td>
								<td style="width: 25%;"><input type="radio"
									ng-model="components.selectedComponent.selectedPath.methods.selectedMethod.content_type"
									value="text/plain"> text/plain </input></td>
							</tr>
						</table>

					</div>
					<br />
					<div ng-show="showQueryParams">
						<div style="width: 100%;">
							<b>QUERY PARAMS</b><br />
							<a class="btn"
									ng-click="addParam()">Add Param</a>
						</div>
						
						<table style="width: 100%;">
							<tr ng-repeat="queryParamsStruct in queryParamsStructs">
								<td style="width: 25%;"><select class="form-control"
									id='EVALUATION_TYPE_ID' ng-change="updateQueryParam()"
									ng-model="queryParamsStruct.key"
									ng-options="queryParam for queryParam in queryParams track by queryParam"></select></td>
									<td style="width: 1%;"></td>
							<td style="width: 25%;">
								<div style="width: 100%;">
									<input type="text" class="form-control" placeholder="value"
										id="key-value-id" ng-model=queryParamsStruct.value ng-change="updateQueryParam()">
								</div>
								<td style="width: 1%;"></td>
							</td>
								<td style="width: 25%;"><a class="btn"
									ng-click="removeParam(queryParamsStruct)">Delete</a></td>
							</tr>
						</table>
					</div>
					<br />
					<div
						ng-show="components.selectedComponent.selectedPath.methods.selectedMethod.type=='POST' || components.selectedComponent.selectedPath.methods.selectedMethod.type=='UPDATE'">
						<div>
							<b>BODY:</b>
						</div>
						<div>
							<textarea id="body-value-id" rows="10" style="width: 100%;">{{components.selectedComponent.selectedPath.methods.selectedMethod.body_file}}</textarea>
						</div>
						<br />
					</div>
					<div>
						<button class="btn btn-primary" ng-click="submitRequest()">SUBMIT
							REQUEST</button>
					</div>
				</div>

			</div>

			<!-- CALL RESPONSE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h2>Response</h2>
				</div>
				<div class="panel-body " ng-show="statusValue">
					<div>
						<table style="width: 100%;">
							<tr>
								<td style="width: 20%;"><b>Status</b></td>
								<td style="width: 1%;"></td>
								<td style="width: 20%;"><div>{{statusValue}}</div></td>
								<td style="width: 5%;"></td>
								<td style="width: 20%;"><b>Type of visualization</b></td>
								<td style="width: 1%;"></td>
								<td style="width: 20%;"><select class="form-control"
									name="visualizationType" id="visualization-type-id"
									ng-options="option for option in responseType.availableTypes track by option"
									ng-model="responseType.selectedResponseType"
									ng-change="updateVisualizationType()"></select></td>
								<td style="width: 13%;"></td>
							</tr>
						</table>
					</div>
					<br />
					<div>
						<div ng-show="visualization_type=='XML'" id="response-body-xml"></div>
						<div ng-show="visualization_type=='RAW'" id="response-body-raw">{{responseValue}}</div>
						<div ng-show="visualization_type=='JSON'" id="response-body-json">
							<pre>{{responseJson}}</pre>
						</div>
						<div ng-show="visualization_type=='PRETTY'"
							id="response-body-pretty">
							<table style="width: 100%;">
								<tr style="width: 100%;">
									<td style="width: 32%;">
										<div class="well" style="width: 100%;">
											<b>RESOURCE URL</b>
										</div>
									</td>
									<td style="width: 2%;"></td>
									<td style="width: 32%;">
										<div class="well" style="width: 100%;">
											<b>GET RESOURCE</b>
										</div>
									</td>
								</tr>

								<tr style="width: 100%;"
									ng-repeat="responseUrl in responseUrls">
									<td style="width: 70%;">
										<div class="panel-body" style="width: 100%;">
											<b>{{responseUrl}}</b>
										</div>
									</td>
									<td style="width: 2%;"></td>
									<td style="width: 28%;">
										<button class="btn btn-primary"
											ng-click="getResource(responseUrl)">GET RESOURCE</button>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<jsp:include page="utils/footer.jsp" />

	<!-- JAVASCRIPT / ANGULAR  -->
	<script src="js/angular.js"></script>
	<script src="js/XMLDisplay.js"></script>

	<script>
		var app = angular.module('SPECS-REST-NEGOTIATION', []);

		app
				.controller(
						'negotiationController',
						[
								'$scope',
								'$http',
								'$filter',
								function($scope, $http, $filter) {
									console.log("angular init");

									$scope.contentTypeEnabled = false;

									$http(
											{
												method : 'GET',
												url : 'services/rest/componentsData?section=negotiation'
											})
											.then(
													function successCallback(
															response) {
														console
																.log(response.data);
														$scope.components = response.data.components;
														setDefaultValues();
													},
													function errorCallback(
															response) {

													});

									function setDefaultValues() {
										$scope.components.selectedComponent = $scope.components[0];
										$scope.components.selectedComponent.selectedPath = $scope.components.selectedComponent.requests[0];
										$scope.components.selectedComponent.selectedPath.methods.selectedMethod = $scope.components.selectedComponent.selectedPath.methods[0];
										$scope.responseType.selectedResponseType = $scope.components.selectedComponent.selectedPath.methods.selectedMethod.visualization_type;
										$scope.urlValue = $scope.components.selectedComponent.base_path+$scope.components.selectedComponent.selectedPath.path;
									
										if ($scope.components.selectedComponent.selectedPath.methods.selectedMethod.query_params != null
												&& $scope.components.selectedComponent.selectedPath.methods.selectedMethod.query_params.length > 0) {
											$scope.showQueryParams = true;
											$scope.queryParams = $scope.components.selectedComponent.selectedPath.methods[0].query_params;
											$scope.queryParamsStructs = [];
										} else {
											$scope.showQueryParams = false;
										}
									}
									
									$scope.addParam = function() {
										if($scope.queryParamsStructs.length == 0){
											$scope.urlApp = $scope.urlValue;
										}
										var queryParamsStruct = {
											"key" : "",
											"value" : ""
										};
										$scope.queryParamsStructs
												.push(queryParamsStruct);
									}

									$scope.removeParam = function(
											queryParamsStruct) {
										var i = $scope.queryParamsStructs
												.indexOf(queryParamsStruct);
										if (i != -1) {
											$scope.queryParamsStructs.splice(i,
													1);
										}
										$scope.updateQueryParam();
									}
									
									$scope.updateQueryParam = function() {
										console.log("updateQueryParam");
										if($scope.queryParamsStructs.length == 0){
											$scope.urlValue = $scope.urlApp;
										}
										for (var i = 0; i < $scope.queryParamsStructs.length; i++) {
											var queryParamsStruct = $scope.queryParamsStructs[i];
											if(i == 0){
												$scope.urlValue = $scope.urlApp+"?"+queryParamsStruct.key+"="+queryParamsStruct.value;
											}else{
												$scope.urlValue = $scope.urlValue+"&"+queryParamsStruct.key+"="+queryParamsStruct.value;
											}
										}
									}

									$scope.updateScreen = function(selectedType) {
										console.log("UPDATED TYPE: "
												+ selectedType);
										if (selectedType == 'component') {
											$scope.components.selectedComponent.selectedPath = $scope.components.selectedComponent.requests[0];
											$scope.components.selectedComponent.selectedPath.methods.selectedMethod = $scope.components.selectedComponent.selectedPath.methods[0];
											$scope.responseType.selectedResponseType = $scope.components.selectedComponent.selectedPath.methods.selectedMethod.visualization_type;
										} else if (selectedType == 'path') {
											$scope.components.selectedComponent.selectedPath.methods.selectedMethod = $scope.components.selectedComponent.selectedPath.methods[0];
											$scope.responseType.selectedResponseType = $scope.components.selectedComponent.selectedPath.methods.selectedMethod.visualization_type;
										} else if (selectedType == 'method') {
											$scope.responseType.selectedResponseType = $scope.components.selectedComponent.selectedPath.methods.selectedMethod.visualization_type;
										}
										if ($scope.components.selectedComponent
												.hasOwnProperty('selectedPath')
												&& $scope.components.selectedComponent.selectedPath.methods
														.hasOwnProperty('selectedMethod')) {
											if ($scope.components.selectedComponent.selectedPath.methods.selectedMethod.body_file_name != "") {
												$http(
														{
															method : 'GET',
															url : 'services/rest/requestBody?file_name='
																	+ $scope.components.selectedComponent.selectedPath.methods.selectedMethod.body_file_name
														})
														.then(
																function successCallback(
																		response) {
																	console
																			.log(response.data);
																	$scope.components.selectedComponent.selectedPath.methods.selectedMethod.body_file = response.data;
																},
																function errorCallback(
																		response) {
																	console
																			.log(response.data);

																});
											}
										}
										$scope.urlValue = $scope.components.selectedComponent.base_path+$scope.components.selectedComponent.selectedPath.path;
									
										if ($scope.components.selectedComponent.selectedPath.methods.selectedMethod.query_params != null
												&& $scope.components.selectedComponent.selectedPath.methods.selectedMethod.query_params.length > 0) {
											$scope.showQueryParams = true;
											$scope.queryParams = $scope.components.selectedComponent.selectedPath.methods[0].query_params;
											$scope.queryParamsStructs = [];
										} else {
											$scope.showQueryParams = false;
										}
									}

									$scope.submitRequest = function() {
										$http(
												{
													method : 'POST',
													url : 'services/rest/submitRequest',
													headers : {
														'Content-Type' : 'application/x-www-form-urlencoded'
													},
													transformRequest : function(
															obj) {
														var str = [];
														for ( var p in obj)
															str
																	.push(encodeURIComponent(p)
																			+ "="
																			+ encodeURIComponent(obj[p]));
														return str.join("&");
													},
													data : {
														'component' : $scope.components.selectedComponent.id,
														'path' :  $scope.urlValue,
														'method' : $scope.components.selectedComponent.selectedPath.methods.selectedMethod.type,
														'body' : document
																.getElementById('body-value-id').value,
														'content-type' : $scope.components.selectedComponent.selectedPath.methods.selectedMethod.content_type
													}

												})
												.then(
														function successCallback(
																response) {
															console
																	.log(response);
															console
																	.log(response.status);
															if(response.status == '200'){
																$scope.responseRaw = response.data;
																$scope.statusValue = $scope.responseRaw.status
																$scope.responseValue = $scope.responseRaw.data
																$scope.responseUrls = $scope.responseRaw.urls
															}else{
																$scope.statusValue = response.status
																+ " "
																+ response.statusText
																$scope.responseValue = $scope.responseRaw.data
															}
															/*
															$scope.statusValue = response.status
																	+ " "
																	+ response.statusText
															console
																	.log($scope.statusValue);
															$scope.responseValue = response.data;
															*/
															$scope
																	.updateVisualizationType();

														},
														function errorCallback(
																response) {
															console
																	.log(response);
															console
																	.log(response.status);
															console
																	.log(response.data);
															$scope.statusValue = response.status
																	+ " "
																	+ response.statusText
															$scope.responseValue = response.data;
															$scope
																	.updateVisualizationType();
														});
									}

									$scope.responseType = {
										availableTypes : [ 'RAW', 'XML', 'JSON', 'PRETTY'  ],
										selectedResponseType : 'RAW'
									};

									$scope.updateVisualizationType = function() {
										if ($scope.responseType.selectedResponseType == 'XML') {
											LoadXMLString('response-body-xml',
													$scope.responseValue);
										} else if ($scope.responseType.selectedResponseType == 'JSON') {
											$scope.responseJson = JSON
													.stringify(
															JSON.parse($scope.responseValue),
															undefined, 4);
										}
										$scope.visualization_type = $scope.responseType.selectedResponseType;
									}
									
									$scope.getResource = function(resourceUrl) {
										$scope.urlValue = resourceUrl;
										$scope.responseType.selectedResponseType = 'RAW';
										$scope.submitRequest();
									}

								} ]);
	</script>
</body>
</html>