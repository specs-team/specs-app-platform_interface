package eu.specsproject.app.model;

public class ChefServerResponse {
	private String code, message, paylod;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPaylod() {
		return paylod;
	}

	public void setPaylod(String paylod) {
		this.paylod = paylod;
	}
}
