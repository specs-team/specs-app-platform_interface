package eu.specsproject.app.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CollectionSchemaV2 {
    private String resource;
    private int total;
    private int members;
    private List<Item> item_list = new ArrayList<>();

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getMembers() {
        return members;
    }

    public void setMembers(int members) {
        this.members = members;
    }

    public List<Item> getItemList() {
        return item_list;
    }

    public void setItemList(List<Item> itemList) {
        this.item_list = itemList;
    }

    public void addItem(Item item) {
        this.item_list.add(item);
    }

    public static class Item {
        private String id;
        private String item;

        public Item() {
        }

        public Item(String id, String item) {
            this.id = id;
            this.item = item;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getItem() {
            return item;
        }

        public void setItem(String item) {
            this.item = item;
        }
    }
}
