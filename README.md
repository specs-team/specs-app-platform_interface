# README #

Platform Interface Description
A SPECS Owner, representing the End-User of this user story, aims at managing all the specs platform.

## Installation ##

**Install using precompiled binaries**

The precompiled binaries are available under the SPECS Artifact Repository (http://ftp.specs-project.eu/public/artifacts/)

Prerequisites:

* Oracle Java JDK 7;
* Java Servlet/Web Container (recommended: Apache Tomcat 7.0.x);
* a running SPECS SLA Platform with Monitoring, Negotiation and Enforcement modules.

Installation steps:

* download the web application archive (war) file from the artifact repository :
http://ftp.specs-project.eu/public/artifacts/applications/platform-interface-app/platform-interface-0.0.1-SNAPSHOT.war
* the war file has to be deployed in the java servlet/web container

If Apache Tomcat 7.0.x is used, the war file needs to be copied into the “/webapps” folder inside the home directory (CATALINA_HOME) of Apache Tomcat 7.0.x.

**Compile and install from source**

The following are the prerequisites to compile and install the Web Container App:

Prerequisites:

* a Git client;
* Apache Maven 3.3.x;
* Oracle Java JDK 7;
* Java Servlet/Web Container (recommended: Apache Tomcat 7.0.x);
* a running SPECS SLA Platform with Monitoring, Negotiation and Enforcement modules.

Installation steps:

* clone the Bitbucket repository:
```
#!bash
o	git clone git@bitbucket.org:specs-team/specs-app-webcontainer-rev2.git
```
* under specs-app-webcontainer-rev2 run:
```
#!bash
mvn install
mvn package
```

The installation generates a web application archive (war) file, under the “/target” subfolder. In order to use the component, the war file has to be deployed in the java servlet/web container. If Apache Tomcat 7.0.x is used, the war file needs to be copied into the “/webapps” folder inside the home directory (CATALINA_HOME) of Apache Tomcat 7.0.x.

### Who do I talk to? ###

* Please contact massimiliano.rak@unina2.it
* www.specs-project.eu

SPECS Project 2013 - CeRICT