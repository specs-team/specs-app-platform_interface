package eu.specsproject.app.model;

/**
 * Photo service user information - added to enable user information to be available to Spring Social client
 *
 * @author Michael Lavelle
 */
public class UserProfile {
	
	private String username;
	private String name;
	private String role;
	
	/**
	 * Create a new PhotoServiceUser
	 *
	 * @param username The unique username for the user
	 * @param name The name of the user
	 */
	public UserProfile(String username, String name, String role)
	{
		this.username = username;
		this.name = name;
		this.setRole(role);
	}

	/**
	 * The unique username for the user
	 *
	 * @return username of the user
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * The name of the user
	 *
	 * @return name of the user
	 */
	public String getName() {
		return name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	

}
