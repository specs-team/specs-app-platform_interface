//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2015.09.23 alle 10:14:23 AM CEST 
//


package eu.specsproject.app.utility;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
public class Collection {

	@XmlAttribute
    public String resource;
    
    @XmlAttribute
    public int total;
    
    @XmlAttribute
    public int items;
    
    @XmlAttribute
    public int members;

    @XmlElement
    public List<ItemList> itemList;
    
    public Collection(){
        //Zero arguments constructor
    }
}
