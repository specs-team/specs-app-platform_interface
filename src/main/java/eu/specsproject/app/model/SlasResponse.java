package eu.specsproject.app.model;

import java.util.List;

public class SlasResponse {
	private String status;
	private String rowResponse;
	private List<SlaDetail> slaDetails;
	
	public SlasResponse(String status, String rowResponse, List<SlaDetail> slaDetails){
		this.status = status;
		this.rowResponse = rowResponse;
		this.slaDetails = slaDetails;
	}
	
	public String getRowResponse() {
		return rowResponse;
	}



	public void setRowResponse(String rowResponse) {
		this.rowResponse = rowResponse;
	}



	public List<SlaDetail> getSlaDetails() {
		return slaDetails;
	}



	public void setSlaDetails(List<SlaDetail> slaDetails) {
		this.slaDetails = slaDetails;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}
}
