package eu.specsproject.app.model;

import java.util.List;

public class Components {
    private List<Component> components;
    
    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }
    
    public class Component {
        private String id;
        private String name;
        private String base_path;
        private List<Request> requests;
        
        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public List<Request> getRequests() {
            return requests;
        }
        public void setRequests(List<Request> requests) {
            this.requests = requests;
        }
        public String getBase_path() {
            return base_path;
        }
        public void setBase_path(String base_path) {
            this.base_path = base_path;
        }
        
    }
    
    public class Request {
        private String path;
        private List<Method> methods;
        
        public String getPath() {
            return path;
        }
        public void setPath(String path) {
            this.path = path;
        }
        public List<Method> getMethods() {
            return methods;
        }
        public void setMethods(List<Method> methods) {
            this.methods = methods;
        }
    }
    
    public class Method {
        private String type;
        private String content_type;
        private String file_path;
        
        public String getType() {
            return type;
        }
        public void setType(String type) {
            this.type = type;
        }
        public String getFile_path() {
            return file_path;
        }
        public void setFile_path(String file_path) {
            this.file_path = file_path;
        }
        public String getContent_type() {
            return content_type;
        }
        public void setContent_type(String content_type) {
            this.content_type = content_type;
        }

    }


    
}
